//
//  VectorQuaternion.h
//  DataCorrection
//
//  Created by User on 10/26/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <simd/simd.h>
@interface VectorQuaternion : NSObject
/**
 * Create quaternion by rotation vector and angle
 *
 * @param rotateVector rotation axis
 * @param angle angle of rotation in positive dimension
 * @return quaternion
 */
+(vector_float4) createQuaternion: (vector_float3) rotateVector angle: (float) angle;
/**
 * Multiply quaternion on scalar
 *
 * @param scale scale variable
 * @param quater quaternion itself
 * @return quaternion
 */
+(vector_float4) scaleQuaternion: (float) scale quaternion: (vector_float4) quater;
/**
 * Get the length of quaternion
 *
 * @param quater quaternion
 * @return length
 */
+(float) lengthQuaternion: (vector_float4) quater;
/**
 * Normalization of quaternion
 *
 * @param quater quaternion to normalize
 * @return normalized quaternion
 */
+(vector_float4) normalizeQuaternion: (vector_float4) quater;
/**
 * Inversion of quaternion
 *
 * @param quater quaternion
 * @return inversed quaternion
 */
+(vector_float4) invertQuaternion: (vector_float4) quater;
/**
 * Multiply quaternions
 *
 * @param quater1 first quaternion
 * @param quater2 second qaternion
 * @return result quaternion
 */
+(vector_float4) multiplyQuaternions: (vector_float4) quater1 secondQuaternion: (vector_float4) quater2;
/**
 * Multiply quaternion by 3D vector
 *
 * @param quater quaternion
 * @param vector3D 3D vector
 * @return multiplied quaternion
 */
+(vector_float4) mul3DVectorQuaternion: (vector_float4) quater vector: (vector_float3) vector3D;
/**
 * Rotate 3D vector using quaternion
 *
 * @param quater rotation quaternion
 * @param vector3D 3D vector to rotate
 * @return rotated 3D vector
 */
+(vector_float3) rotateVector: (vector_float4) quater vector: (vector_float3) vector3D;
@end
