//
//  VectorQuaternion.m
//  DataCorrection
//
//  Created by User on 10/26/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "VectorQuaternion.h"

@implementation VectorQuaternion
+(vector_float4) createQuaternion: (vector_float3) rotateVector angle: (float) angle
{
    rotateVector = vector_normalize(rotateVector);
    vector_float4 quater;

    quater[3] = cosf(angle/2.0);
    quater[0] = rotateVector[0]*sinf(angle/2.0);
    quater[1] = rotateVector[1]*sinf(angle/2.0);
    quater[2] = rotateVector[2]*sinf(angle/2.0);
    return quater;
}
+(vector_float4) scaleQuaternion: (float) scale quaternion: (vector_float4) quater
{
    quater[0] = quater[0]*scale;
    quater[1] = quater[1]*scale;
    quater[2] = quater[2]*scale;
    quater[3] = quater[3]*scale;
    return quater;
}
+(float) lengthQuaternion: (vector_float4) quater
{
//    quat_length = (q.w * q.w + q.x * q.x + q.y * q.y + q.z * q.z) ^ 0.5
    float qLength;
    qLength = sqrtf(quater[0]*quater[0]+quater[1]*quater[1]+quater[2]*quater[2]+quater[3]*quater[3]);
    return qLength;
}
+(vector_float4) normalizeQuaternion: (vector_float4) quater
{
    float n = [self lengthQuaternion:quater];
    quater = [self scaleQuaternion:1.0/n quaternion:quater];
    return quater;
}
+(vector_float4) invertQuaternion: (vector_float4) quater
{
//    res.w = q.w
//    res.x = -q.x
//    res.y = -q.y
//    res.z = -q.z
//    quat_invert = quat_normalize(res)
    quater[3] = quater[3];
    quater[0] = -1*quater[0];
    quater[1] = -1*quater[1];
    quater[2] = -1*quater[2];
    quater = [self normalizeQuaternion:quater];
    return quater;
}

+(vector_float4) multiplyQuaternions: (vector_float4) quater1 secondQuaternion: (vector_float4) quater2;
{
//    res.w = a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z
//    res.x = a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y
//    res.y = a.w * b.y - a.x * b.z + a.y * b.w + a.z * b.x
//    res.z = a.w * b.z + a.x * b.y - a.y * b.x + a.z * b.w
    vector_float4 quater11;
    quater11[3] = quater1[3]*quater2[3] - quater1[0]*quater2[0] - quater1[1]*quater2[1]-quater1[2]*quater2[2];
    quater11[0] = quater1[3]*quater2[0] + quater1[0]*quater2[3] + quater1[1]*quater2[2]-quater1[2]*quater2[1];
    quater11[1] = quater1[3]*quater2[1] - quater1[0]*quater2[2] + quater1[1]*quater2[3]+quater1[2]*quater2[0];
    quater11[2] = quater1[3]*quater2[2] + quater1[0]*quater2[1] - quater1[1]*quater2[0]+quater1[2]*quater2[3];
    return quater11;
}
+(vector_float4) mul3DVectorQuaternion: (vector_float4) quater vector: (vector_float3) vector3D
{
//    res.w = -a.x * b.x - a.y * b.y - a.z * b.z
//    res.x = a.w * b.x + a.y * b.z - a.z * b.y
//    res.y = a.w * b.y - a.x * b.z + a.z * b.x
//    res.z = a.w * b.z + a.x * b.y - a.y * b.x
    vector_float4 quater1;
    quater1[3] = -1*quater[0]*vector3D[0]-quater[1]*vector3D[1]-quater[2]*vector3D[2];
    quater1[0] =quater[3]*vector3D[0]+quater[1]*vector3D[2]-quater[2]*vector3D[1];
    quater1[1] = quater[3]*vector3D[1]-quater[0]*vector3D[2]+quater[2]*vector3D[0];
    quater1[2] = quater[3]*vector3D[2]+quater[0]*vector3D[1]-quater[1]*vector3D[0];
    return quater1;
}
+(vector_float3) rotateVector: (vector_float4) quater vector: (vector_float3) vector3D
{
//    Dim t As TQuat
//    
//    t = quat_mul_vector(q, v)
//    t = quat_mul_quat(t, quat_invert(q))
//    
//    quat_transform_vector.x = t.x
//    quat_transform_vector.y = t.y
//    quat_transform_vector.z = t.z
    vector_float4 tmp;
    tmp = [self mul3DVectorQuaternion:quater vector:vector3D];
    tmp = [self multiplyQuaternions:tmp secondQuaternion:[self invertQuaternion:quater]];
    vector_float3 res;
    res[0] = tmp[0];
    res[1] = tmp[1];
    res[2] = tmp[2];
    return res;
}
@end
