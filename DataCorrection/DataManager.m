//
//  DataManager.m
//  DataCorrection
//
//  Created by User on 10/21/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "DataManager.h"
static const int bufferSize = 20;
#import <malloc/malloc.h>
#import <math.h>
@implementation DataManager
{
    int currentCount;
    int currentPointer;
    float angle;
    
}
+ (id)sharedSingleton {
    static DataManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init ];
    });
    return sharedManager;
}
-(DataManager*) init{
    if (self = [super init]) {
        currentCount = 0;
        currentPointer=-1;
        angle = atan(7/42.0);
    _storageBufferTriangle =malloc(sizeof(Triangle)*bufferSize);
        _logCoord = @"";
    }
    return self;
}

-(void) addPointsTriangle: (float) x1 y1: (float) y1 z1: (float) z1 x2: (float) x2 y2: (float) y2 z2: (float) z2 x3: (float) x3 y3: (float) y3 z3: (float) z3
{
    Triangle tmp;
    tmp.vector[0]=x1;
    tmp.vector[1]=y1;
    tmp.vector[2]=z1;
    
    tmp.vector[3]=x2;
    tmp.vector[4]=y2;
    tmp.vector[5]=z2;
    
    tmp.vector[6]=x3;
    tmp.vector[7]=y3;
    tmp.vector[8]=z3;
    [self addTriangle:&tmp];
}
-(void) addTriangle: (Triangle*) data
{
    
    currentCount++;
    if (currentCount>=bufferSize) {
        currentCount=bufferSize;
    }
    currentPointer++;
    currentPointer %= bufferSize;
   memcpy(_storageBufferTriangle+currentPointer, data, sizeof(Triangle));
    data = [self getLastAddedTriangle];
    [self correctCoord:data];
    vector_float3 n= [self projRotate:*data];
    _logCoord = [[[[[[_logCoord stringByAppendingString:[[NSNumber numberWithFloat: n[0]] stringValue]] stringByAppendingString:@","] stringByAppendingString:[[NSNumber numberWithFloat: n[1]] stringValue]] stringByAppendingString:@","] stringByAppendingString:[[NSNumber numberWithFloat: n[2]] stringValue]] stringByAppendingString:@";\n"];
    //NSLog(@"%f", n[0]);

}
-(void) correctCoord: (Triangle*) data
{
   //  NSLog(@"%f", data->vector[0]);
    for (int c=0; c<9; c++)
    {
        float rec = data->vector[c];
        float middle=0;
        float disp = 0;
        int count = currentCount;
        //mean
        for (int j=0; j<count; j++) {
            middle = middle+_storageBufferTriangle[j].vector[c];
            // NSLog(@"mean");
        }
        
        float s2=0, s=0;
        for (int j=0; j<count; j++) {
            s2+=pow(_storageBufferTriangle[j].vector[c], 2);
            s+=_storageBufferTriangle[j].vector[c];
            
            //  NSLog(@"varianse");
        }
        middle = middle/count;
        disp = (s2-pow(s, 2)/count)/(count-1);
        float meansqr = sqrtf(disp);
        float highBorder=(1-1/currentCount)*middle+1/currentCount*meansqr;
        float lowBorder = (1-1/currentCount)*middle+1/currentCount*meansqr;
        float delta1, delta2, delta = 0.0;
        if (rec>highBorder || rec<lowBorder) {
            delta1 = fabsf(rec-highBorder);
            delta2 = fabsf(lowBorder-rec);
            if (delta1>delta2) {
                delta = delta2;
            }
            else
            {
                delta=delta1;
            }
        }
        //  correct[i] = c;
        if (rec<lowBorder) {
            rec = rec+delta;
        }
        if (rec>highBorder) {
            rec=rec-delta;
        }
       
       // data->vector[c] = rec;
        if (c!=0)
        {
            data->vector[c] = 0.95*data->vector[c-1] + 0.05*data->vector[c];
        }

    }
   //  NSLog(@"%f end", data->vector[0]);
   // return data;
}

-(Triangle*) getLastAddedTriangle{
    return _storageBufferTriangle+currentPointer;
}

-(vector_float3) projRotate: (Triangle) input
{
    vector_float3 firstPoint;
    vector_float3 secondPoint;
    vector_float3 thirdPoint;
    vector_float3 v12, v13 ,v21;
    firstPoint[0] = input.vector[0];
    firstPoint[1] = input.vector[1];
    firstPoint[2] = input.vector[2];
    
    secondPoint[0] = input.vector[3];
    secondPoint[1] = input.vector[4];
    secondPoint[2] = input.vector[5];
    
    thirdPoint[0] = input.vector[6];
    thirdPoint[1] = input.vector[7];
    thirdPoint[2] = input.vector[8];
    
    v12[0] = secondPoint[0]-firstPoint[0];
    v12[1] = secondPoint[1]-firstPoint[1];
    v12[2] = secondPoint[2]-firstPoint[2];
    
    v21[0] = -v12[0];
    v21[1] = -v12[1];
    v21[2] = -v12[2];
    
    v13[0] = thirdPoint[0]-firstPoint[0];
    v13[1] = thirdPoint[1]-firstPoint[1];
    v13[2] = thirdPoint[2]-firstPoint[2];

    vector_float3 v1213 = vector_cross(v12, v13);
    v1213 = vector_normalize(v1213);
//    NSLog(@"V12 %f %f %f", v12[0],v12[1], v12[2]);
//    NSLog(@"V13 %f %f %f", v13[0],v13[1], v13[2]);
//    NSLog(@"V1213 %f %f %f", v1213[0],v1213[1], v1213[2]);
    GLKVector3 vector1213;
    vector1213.x = v1213[0];
    vector1213.y = v1213[1];
    vector1213.z = v1213[2];
    
    GLKVector3 vector12;
    vector12.x = v21[0];
    vector12.y = v21[1];
    vector12.z = v21[2];
  
    vector_float4 quart = [VectorQuaternion createQuaternion:v1213 angle:M_PI-angle];
    v1213 = [VectorQuaternion rotateVector:quart vector:v12];
    

//    v1213 = vector_normalize(v1213);
//    v1213[0] = v1213[0]*1000;
//    v1213[1] = v1213[1]*1000;
//    v1213[2] = v1213[2]*1000;
    secondPoint[0]=firstPoint[0]+v1213[0];
    secondPoint[1]=firstPoint[1]+v1213[1];
    secondPoint[2]=firstPoint[2]+v1213[2];
    v1213[1] = (0-firstPoint[2])*(secondPoint[1]-firstPoint[1])/(secondPoint[2]-firstPoint[2])+firstPoint[1];
    v1213[0] = (0-firstPoint[2])*(secondPoint[0]-firstPoint[0])/(secondPoint[2]-firstPoint[2])+firstPoint[0];
    v1213[2]=0;
    return v1213;
}

@end
