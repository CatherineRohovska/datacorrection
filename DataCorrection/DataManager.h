//
//  DataManager.h
//  DataCorrection
//
//  Created by User on 10/21/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <simd/simd.h>
#import "VectorQuaternion.h"
@import GLKit;
typedef struct{
    float vector[9];
}Triangle;

@interface DataManager : NSObject
@property (atomic,readonly) Triangle* storageBufferTriangle;
@property (atomic, readonly) NSString* logCoord;
/**
 * Gets access to buffer of coordinates. Buffer allocates with const size.
*/
+ (id)sharedSingleton;
/**
 * Adds coordinates of triangle as is. Contain correction of "spikes" inside.
 *
 * @param x1 x-coordinate of first vertex
 * @param y1 y-coordinate of first vertex
 * @param z1 z-coordinate of first vertex
 * @param x2 x-coordinate of second vertex
 * @param y2 y-coordinate of second vertex
 * @param z2 z-coordinate of second vertex
 * @param x3 x-coordinate of third vertex
 * @param y3 y-coordinate of third vertex
 * @param z3 z-coordinate of third vertex
 */
-(void) addPointsTriangle: (float) x1 y1: (float) y1 z1: (float) z1 x2: (float) x2 y2: (float) y2 z2: (float) z2 x3: (float) x3 y3: (float) y3 z3: (float) z3;
/**
 * Gets last added triangle.
 * @return Triangle
 */
-(Triangle*) getLastAddedTriangle;
/**
 * Adds new coordinates as structure
 * @param data Triangle
 */
-(void) addTriangle: (Triangle*) data;

@end
